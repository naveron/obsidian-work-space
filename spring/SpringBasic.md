# Spring基础结构和使用



## Spring基础

Spring 的思想基础是一个bean容器, 可以将自定义实现的bean注入容器中, 提供给其他调用者方便的使用

一般代码构成为 : 

- 表现层, controller
- 业务层, service等逻辑实现
- 持久层, 实现DAO对数据库操作的封装


基于控制反转的思想IOC,  使用反射实现DI依赖注入, 统一处理容器中的对象, 实现核心管理功能, 同时提供各种接口方便开发者扩展  


## SpringIOC容器相关

实现了一个bean容器, 为依赖注入DI提供实现,  spring实现的容器 BEanFactory 和在此基础上的ApplicationContext(BeanFactory子接口实现)  

spring从`xml` 或者其他文件中读取bean定义构造`BeanDefinition` , beanfactory使用bean时实例化bean,  对于不同的bean作用域, signloton和sterotype,  单例模式下容器判断只允许实例化一个类型class的实例, 原型模式下 每次都实例化一个新的bean , 实现不同类型  

Bean的生命周期
获取bean定义构造beandefination 到实例化期间,  有一个实例化过程的干预接口,  bean定义, 实例化, 使用和销毁  

在实际的使用中, 更多的是使用干预接口实现自定义处理, 在之前的`guide-rpc`框架实现中有使用  
- BeanPostProcessor  后置处理器  
	- postProcessBeforeInitialization   实例化之前处理  
	- postProcessAfterInitialization   实例化之后处理  


applicationaware可以实现容器引用被当前bean使用, 一般用来对bean做筛选和其余处理  



## SpringDI相关

一般注入的方式
- 构造函数注入, 在构造函数中传入以来的对象; springboot中直接传入bean类可以自动查找到容器中的实例使用  
- setter注入, 普通方法设置  


## Spring自动装配

根据name装配和根据type  
springboot中使用`@Resource` 和使用`Autowired` 两个注解 实现从容其中装配到当前bean中   

### 基于注解配置

- AutoWired
- Resource   JSR-250规范, 还有postcontrust  --> 实例化阶段被调用,  preDestory 注解
- Qualifier  两个同类型不同名称的bean 
- Primary  某个接口的主要实现 
- 等注解 

Autowired 属于spring注解, 可以用在构造器, set方法, 属性/字段上  
List 实现了接口的所有bean,  Map 获取key接口的所有实现   


## Spring事件处理

> 通过 ApplicationEvent 类和 ApplicationListener 接口来提供在 ApplicationContext 中处理事件


## Spring AOP

Aspect of part 
@Aspect 注解 @Before  @After  @Around 定义切面和执行的handler  




## Spring 事务管理

PlatformTransactionManager  编码式使用事务,  或者使用@Transaction 注解  --> 声明式事务 










