# Raft 一致性算法  

分布式一致性 : 在一个集群中多个节点中的数据保证一致性 的算法  

term  : 任期.  选举产生leader, 发生宕机后重新选举产生leader  
大多数节点 : 超过 N/2+1个节点投票选举产生leader  
状态/身份 : leader  follower  candidate(候选人)  

## 选举过程  

每个节点分配一个随机时间, 等待投票,  等待时间最少的成为候选人, 发送消息到另外节点 表示投票给自己, 最周统计投票信息, 最多成为leader  


## 数据同步  

client --> set data --> leader --> uncommit  --> 同步给其他节点 --> 多数返回确认后 --> 执行commit 确认提交, 返回客户端  

## 网络分区/脑裂 

因为网络原因, 两个raft集群产生2个leader , 刚好形成2个集群, 



> https://www.cnblogs.com/bangerlee/p/5655754.html  分布式理论  






