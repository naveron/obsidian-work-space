## 集度面试补充重点学习的点  

kafka ISR Rebalance Repatation   
安全生产  
写入和消费是怎么保证并发和高效的  

事务和缓存
Mysql 数据存储结构, 怎么保存数据的 ?   
Zookeeper 协调服务, 理解就好, 重点放在kafka各种实现细节  
- 零拷贝  
- 批量发送  
- 如何增加生产接收能力和消费能力  

Java 对象从产生到回收的过程  
为什么对象加载需要双亲委派  
gc root 根节点标记, 为什么需要三次阶段标记, 为什么第三次需要再次回收? 之后回收不也是一样的吗  
CMS  G1 垃圾回收  
spring 切入点, AOP的实现  
如何实现项目中敏感点的执行效率监控  
spring starter redis starter 怎么实现的, 整个过程  

HashMap 头插和尾插, 并发  
kafka 怎么安全监控的  








