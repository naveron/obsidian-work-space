# GoLang 第一课

## 环境搭建 

一般使用IntelliJ 家族的Goland开发环境, 需要付费使用  

免费的IDE有 :  
- VsCode  需要额外配置和下载插件   
- [LiteIDE X]( http://liteide.org/cn/  )  

## go 语言基本结构  

> 包的声明 package xxx 
> 导入包 import "xx" 或 { "", ""} 
> 主函数  

![[assets/Pasted image 20230305111022.png]]

> Go 语言中是以大小写作为标识区分的：若以大写字母开头，则为可导出（对外公开）；若为小写字母，则为非导出（对外不公开）属性

```go
// 可导出，允许外部包引用

type T struct {}

// 非导出，仅允许当前包引用

type t struct {}

```


## go 高级语言特性  

channel 的使用, 相当于简单队列的实现  
defer 延迟执行  
协程  go xxx 
抛出异常 panic , 配合recover 和 refer  实现捕获和处理, 继续执行  

```go 
func main() {

	defer func() {

		if r := recover(); r != nil {

			fmt.Printf("recover: %s", r)

		}

	}()

	panic("脑子进煎鱼了")

}

// 因为defer修饰, main函数返回的时候才会执行, 所以捕获panic 异常  
```

## 学习大纲 

![[assets/Pasted image 20230305155607.png]]







