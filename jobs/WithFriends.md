

# 如何于别人相处 

## 如何作为一个优秀的人

```
最近接触过的优秀的人的几个共同特点：

1.坦荡慷慨乐于分享，因为懂得深入交流一定是对等的，给予能够换来更多回馈；

2.勤于且善于思考，有随手记录并定期总结梳理信息的习惯；

3.遇到问题不拘泥纠结于具体细微的解决手段，先思考底层基础规律，思维开阔举一反三；

4.自我驱动力强，遇到问题习惯于积极寻求解决方案，不会停下来等指示，更不会甩锅；

5.遇到优秀的人不会眼红嫉妒，瞅着对方的短板缺陷说“其实他也就那回事”，而是懂得欣赏对方的优点，乐于向对方学习；

6.人前直率坦诚给对方提出建议，人后向其他朋友真诚赞美对方的优点。大部分人是反着来。

```









