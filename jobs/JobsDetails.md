的优秀的人的几个共同特点# 招聘信息

根据岗位要求针对性的学习和实现 练习等， 主动投递简历， 万一成功了呢， 另外找寻其他的出路  


## 知乎 Java开发 北京

```
职位描述

-   工作描述：
-   1. 参与交易平台研发及功能迭代，深入发掘和分析业务需求，撰写技术方案和系统设计，以及相关的代码开发. 2. 分析和发现系统的优化点，负责推动系统的性能和可用性的提升； 3. 参与 Java 组件的设计和研发，包括日志，数据库，RPC 等等。 4. 参与支付，商品，订单，物流，库存，结算，营销以及售后等子系统的研发工作。 5. 为团队引入创新的技术、创新的解决方案，用创新的思路解决问题。
-   任职要求：
-   1、计算机相关专业，本科以上学历，一年以上工作经验，有过电商相关产品研发经历的优先； 2. 熟悉Linux 开发环境，扎实的 JAVA 基础，具备优秀的编码能力，复杂业务系统研发经验； 3. 具有出色的抽象设计能力，能独立分析和解决问题 4. 掌握基本的设计原则和常用的设计模式，深刻理解系统的可读性，可维护性以及健壮性 5. 熟悉 Spring、Mybatis、Mysql、Redis、ES、Kafka 等组件 6. 有产品意识，能技术侧输出观点，关注产品效果 7. 有很好的分析问题和解决问题的能力，责任心强，善于学习，善于沟通和表达，有良好的团队合作意识
```


## TDEngine 做流式数据库的开发

方向比较好， 值得深入研究  , 寻找志同道合的朋友  


```
### 资深 Java 工程师（高并发方向）

**工作职责：**

1.  负责数据库云服务平台关键部件的研发工作，包含技术选型、架构设计、开发实现；
2.  解决云服务体系的高并发性能问题，在云服务容器的基础上，保证系统整体的高可用性和弹性扩展；
3.  围绕云服务生态，分析相关组件中存在的问题，并形成解决方案。

**基本条件：**

1.  5年以上工作经验，具备十万级/秒线上生产系统的研发经验；
2.  编程功底扎实，深入理解高并发后端系统的技术要点；
3.  精通Java语言，能对C/C++代码做简单维护和调用；
4.  熟悉GO、Scala、Rust等新兴语言，并使用这些语言进行过生产系统研发者优先；
5.  了解传统数据库、主流大数据平台软件的基本原理，具备在生产系统中应用Docker或Kubernetes容器的使用经验；
6.  独立工作能力强，对新兴技术潮流兴趣浓厚，能分析、拆解复杂的技术问题，攻克难关；
7.  有自己创建或深度参与的开源项目，GitHub高星者优先。
```









