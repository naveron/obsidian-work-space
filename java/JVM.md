# 关于java虚拟机

## 运行时

### 线程私有
方法计数器, 当前执行的位置;  
虚拟机栈, 每次调用封装成方法帧, 压栈处理, 返回后出栈;  
本地方法栈 : 调用本地操作系统的底层调用  JNI  

### 堆
新创建的java对象, 临时变量, 常量池等  
请求的压栈数据过多 会抛出 stackoverflow错误  
需要申请内存, 但内存不够 会抛出outofmemory错误  

### 方法区/meta/永久代

class 载入jvm的数据/meta信息  
放在本地内存中, 不进行垃圾回收, 提高访问速度  
1.8之前常量池放在永久代, 之后放在堆中, 进行常规的垃圾回收, 会变成老年代  


### 直接内存  

Java NIO 使用DirectByteBuffer 对象提供了堆堆外内存的直接访问, 不需要在堆内堆外来回复制, 主要提高效率  

## 垃圾标记和回收

### 垃圾是否可以被回收  

引用计数法  
可达性分析  -- > 哪些对象可以作为gc root根节点 ?  

### 对象引用类型  

强引用  软引用(当内存不够时优先回收)   弱引用(只要进行垃圾回收就会被回收, threadlocal 实现继承自weakreference )  虚引用 (一般用来统计垃圾回收的情况, 垃圾回收时受到一个通知)   

### 垃圾回收算法  

- 标记清除
- 标记整理, 标记后, 移动到连续内存 
- 标记复制, 分为两块区域, 标记清除后, 依次全部复制到另一个区域, 依次往复  
- 不同的周期对象使用不同的回收算法. 新生代 : 标记清除算法, 因为生命周期短, 快速生成和消除;  老年代 : 标记整理/复制算法  


### 实现的垃圾回收器  

![[assets/Pasted image 20220903093839.png]]  

serial 收集器, 单线程执行垃圾回收  --> serial old 
parnew收集器, serial 的多线程版本, 垃圾回收时多线程执行回收;  并行执行新生代垃圾处理, 老年代单线程处理  --->  parnew  old收集器
Parallel Scavenge 收集器 , 缩短新生代垃圾回收时间, 减少jvm的停顿, 尽可能保证主程序正常运行.  吞吐量优先  
CMS 收集器. 
> 分为以下四个流程：
> 初始标记：仅仅只是标记一下 GC Roots 能直接关联到的对象，速度很快，需要停顿。
> 并发标记：进行 GC Roots Tracing 的过程，它在整个回收过程中耗时最长，不需要停顿。
> 重新标记：为了修正并发标记期间因用户程序继续运作而导致标记产生变动的那一部分对象的标记记录，需要停顿。
> 并发清除：不需要停顿。  

G1 收集器 :   
![[assets/Pasted image 20220903095953.png]]  
将回收区域分块/较大的区域region  
每个 Region 都有一个 Remembered Set，用来记录该 Region 对象的引用对象所在的 Region。  
通过使用 Remembered Set，在做可达性分析的时候就可以避免全堆扫描  
> 初始标记
> 并发标记
> 最终标记：为了修正在并发标记期间因用户程序继续运作而导致标记产生变动的那一部分标记记录，  
> 虚拟机将这段时间对象变化记录在线程的 Remembered Set Logs 里面，最终标记阶段需要把 Remembered Set Logs 的数据合并到 Remembered Set 中。  
> 这阶段需要停顿线程，但是可并行执行。  
> 筛选回收：首先对各个 Region 中的回收价值和成本进行排序，根据用户所期望的 GC 停顿时间来制定回收计划。  
> 此阶段其实也可以做到与用户程序一起并发执行，但是因为只回收一部分 Region，时间是用户可控制的，而且停顿用户线程将大幅度提高收集效率。  


## 内存和回收策略  

minor GC 和major GC, 一般minor GC比较快,  针对新生代对象,,  major GC 针对堆中所有对象, 包括新生代和老年代, 会造成停顿,
如果频繁major GC说明虚拟机配置问题或程序问题  
对象优先在eden 区分配;  大对象直接进入老年代区;  空间分配担保 : 如果老年代可用空间大于新生代对象, 直接minor GC, 
如果不足够, 检查当前老年代空间是否大于历次晋升对象的空间, 如果还不足够, 进行major GC/ Full GC  
> 如何处罚full gc : 
> 1. 调用system.gc() ;  
> 2. 老年代空间不足  
> 3. 空间担保分配失, 也就是空间不足导致的  
> 4. cms过程, 有对象进入老年代, 老年代空间不足, 抛出异常 

## 类加载机制  

加载验证准备解析初始化使用卸载

类初始化时机 : 主动实例化一个对象,  反射创建一个对象 ;  使用静态类常量不会被初始化  


## 类加载器

从Java虚拟机角度看 : 启动加载器 bootstrap  classloader, 是虚拟机的一个一个实现/C++   
Java开发者看 : 
启动加载器, 扩展加载器, 应用加载器 BootstrapClassLoader,  ExtersionClassLoader,  Application ClassLoader    

经典双亲委派机制  
![[assets/Pasted image 20220903114611.png | 600]]  

加载器会将加载请求交给父类加载器加载, 父类无法完成, 返回自己加载, 凡是能够在某一层实现加载 就直接加载, 这样可以保证 一定是上级加载器无法完成加载,  才交给下级  

自定义实现类加载器  :  

```Java
public class FileSystemClassLoader extends ClassLoader {
    private String rootDir;
    public FileSystemClassLoader(String rootDir) {
        this.rootDir = rootDir;
    }
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] classData = getClassData(name);
        if (classData == null) {
            throw new ClassNotFoundException();
        } else {
            return defineClass(name, classData, 0, classData.length);
        }
    }
    private byte[] getClassData(String className) {
        String path = classNameToPath(className);
        try {
            InputStream ins = new FileInputStream(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int bufferSize = 4096;
            byte[] buffer = new byte[bufferSize];
            int bytesNumRead;
            while ((bytesNumRead = ins.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesNumRead);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private String classNameToPath(String className) {
        return rootDir + File.separatorChar
                + className.replace('.', File.separatorChar) + ".class";
    }
}
```








