# Java 多线程理论

## AQS 

虚拟队列同步器, 用来解决多个线程获取锁之间的消息传递和状态感知  
提供一个资源标志, 表示当前的资源是否在使用, 被哪个线程占用,  资源标志可以有多个, 表示可以提供给多个线程使用, 直到使用完  

原理 : 如何实现这种同步器/协调器  
线程首先尝试获取锁，如果失败就将当前线程及等待状态等信息包装成一个node节点加入到FIFO队列中. 
接着会不断的循环尝试获取锁，条件是当前节点为head的直接后继才会尝试。
如果失败就会阻塞自己直到自己被唤醒。而当持有锁的线程释放锁的时候，会唤醒队列中的后继线程。  


### 独占模式AQS

> 当前资源只允许一个线程使用, 其他线程必须等待排队或者直接放弃,  ReentrantLock 实现,  只允许一个线程访问资源, 其余排队    
> 独占模式下的AQS是不响应中断的 ，指的是加入到同步队列中的线程，
> 如果因为中断而被唤醒的话，不会立即返回，并且抛出InterruptedException。
> 而是再次去判断其前驱节点是否为head节点，决定是否争抢同步状态。如果其前驱节点不是head节点或者争抢同步状态失败，那么再次挂起  

![[assets/Pasted image 20220902211227.png]]

整个过程包括获取资源和释放资源  

### 共享模式AQS  

> 很明显，我们可以将state的初始值设为N（N > 0），表示空闲。
> 每当一个线程获取到同步状态时，就利用CAS操作让state减1，直到减到0表示非空闲，其他线程就只能加入到同步队列，进行等待。
> 释放同步状态时，需要CAS操作，因为共享模式下，有多个线程能获取到同步状态。CountDownLatch、Semaphore正是基于此设计的。  

![[assets/Pasted image 20220902213941.png]]




