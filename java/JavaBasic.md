# Java基础

## javassist使用

javassist实现操作字节码, 反编译为class类的功能, CtClass对象, CtMethod, CtField 对应Class  Method  Field , 可以自定义在代码中创建一个java文件  

反射的使用  
constructure  method  field 获取
setAccessable 设置范围域  


## Java异常

可捕获的异常和运行时异常  
一般exception可捕获, 不会造成灾难后果, Error 比较严重的jvm异常  
![[error_exception.png | 700]]

![[error_exception2.png]]


## 注解

自带的注解  常用注解 
- @Override，表示当前的方法定义将覆盖超类中的方法。
- @Deprecated，使用了注解为它的元素编译器将发出警告，因为注解@Deprecated是不赞成使用的代码，被弃用的代码。
- @SuppressWarnings，关闭不当编译器警告信息。

### 自定义实现注解 

```Java
@Target(ElementType.METHOD)  // 方法上使用
@Retention(RetentionPolicy.RUNTIME)  // 生命周期   -- > 保留在什么时候 
@Document
@Inherent  // 可继承 直接注解在子注解中 
public @interface annotation {
	public String name() default "default";
	public int age() default 0;  // 不能设置为null  注解时必须有值  
}
```

> @Target ：
> 表示该注解可以用于什么地方，可能的ElementType参数有：
> CONSTRUCTOR：构造器的声明
FIELD：域声明（包括enum实例）
LOCAL_VARIABLE：局部变量声明
METHOD：方法声明
PACKAGE：包声明
PARAMETER：参数声明
TYPE：类、接口（包括注解类型）或enum声明
CONSTRUCTOR：构造器的声明
FIELD：域声明（包括enum实例）
LOCAL_VARIABLE：局部变量声明
METHOD：方法声明
PACKAGE：包声明
PARAMETER：参数声明
> TYPE：类、接口（包括注解类型）或enum声明

> @Retention ：
表示需要在什么级别保存该注解信息。可选的RetentionPolicy参数 包括：
SOURCE：注解将被编译器丢弃
CLASS：注解在class文件中可用，但会被VM丢弃
RUNTIME：VM将在运行期间保留注解，因此可以通过反射机制读取注解的信息。

> 
> @Document ：
将注解包含在Javadoc中
@Inherited ：
允许子类继承父类中的注解


一般自定义注解需要配合注解解析动作进行, 比如使用aop切面的方式扫描所有包下的类, 获取当前类/方法上的注解, 是否满足条件等  


## Java容器

jdk自带的实现, 如arraylist  linkedlist  map接口实现, hashmap, linkedmap  queue接口--> 实现priorityqueue, dequeue, 
一般queue建议使用linkedlist 结构, 也是实现了queue接口的  

- ArrayList  ? Vector 一般不使用 , 多线程结构使用collections.synchronizedList 方法包装或直接使用 CopyOnWriteList  
- LinkedList  指针链表 
- CopyOnWriteList  读多写少的情况下   更新时复制一个数组, 结束时将数组指针更换成新的数组地址, 使用了retrentlock 保证并发访问  
- HashMap    ConcurrentHashMap   1.7 使用segment 双层锁增大并发度, 1.8 使用Node 小粒度锁实现
- LinkedHashMap  继承自 hashmap, 内部使用了linkedlist双向链表  
- weakhashmap  之前没有接触过的, weakhashcache使用这个结构实现缓存, weakhashmap继承自weakreference 


## Java 多线程和并发

### Thread创建和销毁

创建线程
- 实现runnable接口 , 传入thread构造函数, start()
- 继承Thread实现自定义线程实现
- 其他的还有callable,  如futuretask实现

结束一个线程的方法, stop比较暴力 ,强制终止当前的线程, 如果设置可重入锁会导致锁无法释放, 后果严重, 一般开发者手动控制, 保证try catch  final 结构, 
线程中有一个interecpt标志, setinterecpt表示当前线程需要终止, 线程的死循环需要对此判断, 对于sleep  wait的线程抛出异常  

关于为什么线程取消了stop resume等方法, 说明 : [Java Thread Primitive Deprecation (oracle.com)](https://docs.oracle.com/javase/1.5.0/docs/guide/misc/threadPrimitiveDeprecation.html)  
会导致锁释放出现问题  
interrecpt 给线程一个信号, 在thread内部的一个变量, 表示外部希望终结当前线程  


### 线程同步

- synchronized  锁住一个资源对象, 使用在方法上表示锁住当前class  , 锁住this表示当前实例不能多个线程访问,  锁住xxx.class表示锁住类, 这个类的所有实例不能同时访问这段代码  
- rentrantLock 各种锁的实现, 属于一种互斥锁 
- wait notify 的同步机制 

多线程之间的合作

- join,  比如fork  join架构的使用, xxl-job项目中, 结束一个demon循环线程的时候, 除了使用interecpt外, 还是用join等待子线程方法执行完毕    
- object 中wait  notify方法, 可以协调2个线程之前的关系  
- retrantLock使用condition 可以创建多个条件锁, 保证多个线程之间的执行顺序  

### 线程的状态 
NEW RUNNABLE  BLOCKED  WAITING  TIME_WAITING  TERMINATED  



## 结构性的多线程并发组件 JUC 

JUC 工具包中许多多线程结构和工具实现
- concurrentHashMap 
- AQS  基于此实现的有RetrantLock   Semaphere 信号量
- countDownLatch  倒计时, 主线程等待其他线程完成 调用countdown方法 , 继续向后进行  
- CyclicBarrier  累加器循环  当多个线程都到达时, 先后进行, 通过reset可以循环使用, CountDownLatch只能使用一次 
> 我的感觉, countdownlatch 是子线程通知主线程, 可以向下进行了, cyclicBarrier 是子线程调用await后等待主线程, 数量到齐后 ,子线程都可以继续向后进行, 类似于栅栏    
- semaphere 信号量 

创建线程任务的组件
- futuretask 实现Runnable Future 接口, 
- blockingqueue  阻塞队列, 支持多线程 , 提供了阻塞方法 take  put 
- fork-join 框架, 类似于c语言中的fork 


### 线程数据共享无同步方案

ThreadLocal  thread线程中保存map, 使用threadlocal生成key, 保存数据  
局部变量,   赋值给局部变量, 返回后更新全局的数值  


### 锁的优化 

自旋锁   获取不到锁后,  循环等待一会再次获取, 适用于竞争不激烈的情况下  
锁粗化,  某些情况下大量的上锁和释放导致性能消耗,  对一段操作同时加一个锁, 操作完成后释放  
轻量级锁 ---> 偏向锁和轻量级锁  
> 轻量级锁是相对于传统的重量级锁而言，它使用 CAS 操作来避免重量级锁使用互斥量的开销。对于绝大部分的锁，在整个同步周期内都是不存在竞争的，因此也就不需要都使用互斥量进行同步，可以先采用 CAS 操作进行同步，如果 CAS 失败了再改用互斥量进行同步。
> 当尝试获取一个锁对象时，如果锁对象标记为 0 01，说明锁对象的锁未锁定（unlocked）状态。此时虚拟机在当前线程的虚拟机栈中创建 Lock Record，然后使用 CAS 操作将对象的 Mark Word 更新为 Lock Record 指针。如果 CAS 操作成功了，那么线程就获取了该对象上的锁，并且对象的 Mark Word 的锁标记变为 00，表示该对象处于轻量级锁状态。

## 多线程开发实践

- 线程开发时给定一个id名称  
- 尽量缩小锁的范围,  锁越小, 约到底层, 所得次数越少, 开销越小  
- wait  notify  countdownlatch  cycliccarrier   semaphere信号量 synchronized 等
- 使用blockingqueue  实现简单的生产者消费者模型  
- 使用局部变量/本地变量和不可变变量实现数据传递, 这样可以减少消耗  
- 业务性批量的创建线程/周期创建  使用线程池, 更好的管理线程和利用计算机资源  







