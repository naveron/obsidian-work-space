# Go 开发基础指导
> [# How to Write Go Code](https://go.dev/doc/code)  

## 介绍  

这个文档演示了一个在模块中开发一个简单go package的开发过程, 以及介绍使用 `go tool`, 获取 构建 安装go 模块的标准方法  
标注 : 这个文档假定你使用 go 1.13或更新, 并且 GO111MODULE 变量没有设置, 如果你在找更老的 之前模块版本文档, 请参见 [这里](https://go.dev/doc/gopath_code.html)    

## 代码组织 
go 以文件作为package 路径, 在一个文件夹下的属于一个`package`, 在一个资源文件里定义的函数, 类型, 变量和常量对于在同一个package中的其他资源可见  
go仓库一般只包含一个module, 一个模块下有一个`go.mod`文件表示这个是仓库module的根, 
导入包路径方法, 如一个module 是 `github.com/google/go-cmp`需要导入其下的`cmp`文件夹下的资源, 导入语法需要写成 : `github.com/google/go-cmp/cmp`, 在标准库中的packages没有模块路径前缀  

## 第一个程序  
在一个路径下创建`go.mod`  
```shell
$ mkdir hello # Alternatively, clone it if it already exists in version control.
$ cd hello
$ **go mod init example/user/hello**
go: creating new go.mod: module example/user/hello
$ cat go.mod
module example/user/hello

go 1.16
$
```

go源文件必须先声明 package, 执行命令**必须使用main package**  
创建源文件`hello.go`, 如下go代码  
```go
package main

import "fmt"

func main() {
    fmt.Println("Hello, world.")
}
```

使用go命令编译安装  `go install example/user/hello`, 安装的是整个module , 编译的go文件会放在用户文件夹下 `go/bin/...`路径下  
安装路径在GOPATH和GOBIN中环境配置, 如果GOBIN设置, bin文件会安装到那个文件夹下, 如果GOPATH设置, bin会安装到路径的子文件夹下  
设置go环境 `go env -w GOBIN=/somewhere/else/bin`, 重设`go env -u GOBIN`, 实现变更环境变量  
命令行必须在有`go.mod`文件同路径下执行, 否则会出错 
方便起见, go命令接受相对路径, 并且默认是当前路径  ; 可以添加go的输出bin路径到全局变量, 这样可以直接运行bin程序  
go项目可以构建一个仓库, 一些主机服务提供metadata, 可以使库方便的被别人使用  


## 从你的模块导入包  
接着从之前的项目开始. 创建一个目录morestring, 路径下创建`reverse.go`文件:  
```go
// Package morestrings implements additional functions to manipulate UTF-8
// encoded strings, beyond what is provided in the standard "strings" package.
package morestrings

// ReverseRunes returns its argument string reversed rune-wise left to right.
func ReverseRunes(s string) string {
    r := []rune(s)
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return string(r)
}
```

> go 中, 大写开头的是public, 小写是私有的  

`ReverseRunes` 是大写开头的 , 可以被其他的源文件使用, 编译文件 `go build`, 并在其后使用  
修改原来的hello文件并使用`morestring`包  
```go
package main

import (
    "fmt"
    "example/user/hello/morestrings"
)

func main() {
    fmt.Println(morestrings.ReverseRunes("!oG ,olleH"))
}

// 然后执行 安装命令并运行 
go install xxxxxx
```


## 从远程模块导入包package 

直接使用cmp包  
```go 
package main

import (
    "fmt"

    "example/user/hello/morestrings"
    "github.com/google/go-cmp/cmp"
)

func main() {
    fmt.Println(morestrings.ReverseRunes("!oG ,olleH"))
    fmt.Println(cmp.Diff("Hello World", "Hello Go"))
}
```
需要下载和在mod.go中记录  
`go mod tidy` 命令添加缺失的模块  
```shell
$ go mod tidy
go: finding module for package github.com/google/go-cmp/cmp
go: found github.com/google/go-cmp/cmp in github.com/google/go-cmp v0.5.4
$ go install example/user/hello
$ hello
Hello, Go!
  string(
-     "Hello World",
+     "Hello Go",
  )
$ cat go.mod
module example/user/hello

go 1.16

**require github.com/google/go-cmp v0.5.4**
$
```
go命令会自动下载到pkg/mod文件夹下, 只读, 如果需要删除, 使用 `go clean -modcache` 命令  

## Test 测试  
go 有一个轻量的测试框架, 写一个go文件, 以`_test.go`为结尾, 里面包含形如`TestXXX`的方法  
创建一个测试文件`reverse_test.go`包含如下的代码  

```go 
package morestrings

import "testing"

func TestReverseRunes(t *testing.T) {
    cases := []struct {
        in, want string
    }{
        {"Hello, world", "dlrow ,olleH"},
        {"Hello, 世界", "界世 ,olleH"},
        {"", ""},
    }
    for _, c := range cases {
        got := ReverseRunes(c.in)
        if got != c.want {
            t.Errorf("ReverseRunes(%q) == %q, want %q", c.in, got, c.want)
        }
    }
}

// 使用 go test 命令执行测试 
$ cd $HOME/hello/morestrings
$ **go test**
PASS
ok  	example/user/hello/morestrings 0.165s
$

```




# go实现一个class  

> [# Codewalk: First-Class Functions in Go](https://go.dev/doc/codewalk/functions/)  

## 源代码  

```go
// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main  // 声明 package  

import (  // 导入包 
	"fmt"
	"math/rand"
)

const (  // 声明 常量 
	win            = 100 // The winning score in a game of Pig
	gamesPerSeries = 10  // The number of games per series to simulate
)

// A score includes scores accumulated in previous turns for each player,
// as well as the points scored by the current player in this turn.
type score struct {   // 定义一个数据结构, 相当于 class  
	player, opponent, thisTurn int  // 声明 int 类型的三个变量 
}

// An action transitions stochastically to a resulting score.
type action func(current score) (result score, turnIsOver bool)  // 只定义函数接口, 没有实现, 函数名为 action 

// roll returns the (result, turnIsOver) outcome of simulating a die roll.
// If the roll value is 1, then thisTurn score is abandoned, and the players'
// roles swap.  Otherwise, the roll value is added to thisTurn.
func roll(s score) (score, bool) {  // 滚动更新 score 分数 , 这个是 action的实现, action相当于接口的定义 
	outcome := rand.Intn(6) + 1 // A random int in [1, 6]
	if outcome == 1 {
		return score{s.opponent, s.player, 0}, true
	}
	return score{s.player, s.opponent, outcome + s.thisTurn}, false
}

// stay returns the (result, turnIsOver) outcome of staying.
// thisTurn score is added to the player's score, and the players' roles swap.
func stay(s score) (score, bool) {  // 实现 action接口, 传入score, 返回 score 和 bool 
	return score{s.opponent, s.player + s.thisTurn, 0}, true
}

// A strategy chooses an action for any given score.
type strategy func(score) action  // 定义函数, 传入 score, 返回action函数 --> 定义接口形式 
// 这个接口定义 参数没有 变量名称, 返回同 

// stayAtK returns a strategy that rolls until thisTurn is at least k, then stays.
func stayAtK(k int) strategy {  // 返回strategy 接口函数, 实现的那些函数 都可以作为返回函数 
	return func(s score) action {
		if s.thisTurn >= k {
			return stay
		}
		return roll
	}
}

// play simulates a Pig game and returns the winner (0 or 1).
func play(strategy0, strategy1 strategy) int {  // 传入2种strategy, 返回int值 
	strategies := []strategy{strategy0, strategy1}
	var s score
	var turnIsOver bool
	currentPlayer := rand.Intn(2) // Randomly decide who plays first
	for s.player+s.thisTurn < win {
		action := strategies[currentPlayer](s)
		s, turnIsOver = action(s)
		if turnIsOver {
			currentPlayer = (currentPlayer + 1) % 2
		}
	}
	return currentPlayer
}

// roundRobin simulates a series of games between every pair of strategies.
func roundRobin(strategies []strategy) ([]int, int) {  // 返回int数组和 int 
	wins := make([]int, len(strategies))
	for i := 0; i < len(strategies); i++ {
		for j := i + 1; j < len(strategies); j++ {
			for k := 0; k < gamesPerSeries; k++ {  // 每2种策略 循环计算 
				winner := play(strategies[i], strategies[j])
				if winner == 0 {
					wins[i]++
				} else {
					wins[j]++
				}
			}
		}
	}
	gamesPerStrategy := gamesPerSeries * (len(strategies) - 1) // no self play
	return wins, gamesPerStrategy
}

// ratioString takes a list of integer values and returns a string that lists
// each value and its percentage of the sum of all values.
// e.g., ratios(1, 2, 3) = "1/6 (16.7%), 2/6 (33.3%), 3/6 (50.0%)"
func ratioString(vals ...int) string {
	total := 0
	for _, val := range vals {  // 求和 
		total += val
	}
	s := ""
	for _, val := range vals {  // 计算 占比 
		if s != "" {
			s += ", "
		}
		pct := 100 * float64(val) / float64(total)
		s += fmt.Sprintf("%d/%d (%0.1f%%)", val, total, pct)
	}
	return s
}

func main() {
	strategies := make([]strategy, win)
	for k := range strategies {
		strategies[k] = stayAtK(k + 1)
	}
	wins, games := roundRobin(strategies)

	for k := range strategies {
		fmt.Printf("Wins, losses staying at k =% 4d: %s\n",
			k+1, ratioString(wins[k], games-wins[k]))
	}
}

```


## 关键实现解析  

变量的定义 var xx  int/string   
数组定义  var x []int  
接口函数的定义, 只有名称没有实现 type xxx func(xx int) int  或者 type xxx func(int) int  
接口函数的实现, 参数一致, 返回一致即可  
返回函数  return func(xx)xx {  xxx ...  }  返回实现即可, 上层函数返回值 写接口名称  


















